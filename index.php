<?php
if(isset($_POST['submit'])) {
	
	/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
	
	include("class.phpmailer.php");
	extract($_POST);
	$mail = new phpmailer();
	
	/*$mail->IsSMTP();
	$mail->SMTPAuth = true;
	$mail->Host = "smtp.mailgun.org"; // SMTP a utilizar. Por ej. smtp.elserver.com
	$mail->Username = "contacto@elhogardelcastor.com"; // Correo completo a utilizar
	$mail->Password = "2WUWkv7J"; // Contraseña
	$mail->Port = 587;*/
	 
	$mail -> From = "correo@daltoncorporacion.com.mx/";
	$mail -> FromName = "Dalton Talento"; 
	$mail -> Subject = "Nuevo mensaje:"; 
	
	$mail -> AddAddress ("maria.oropeza@dalton.com.mx"); //
	$mail -> Body = "
	<div style='width:100%;margin:0;padding:0;background-color:#f5f5f5;font-family:Arial,sans-serif' marginheight='0' marginwidth='0'>
<div style='display:block;min-height:5px;background-color:#f00707'></div>
<center>
<table width='100%' height='100%' cellspacing='0' cellpadding='0' border='0'>
    <tbody>
        <tr>
        <td valign='top' align='center' style='border-collapse:collapse;color:#525252'>
            <table width='85%' cellspacing='0' cellpadding='0' border='0'>
                <tbody>
                    <tr>
                    	<td valign='top' height='20' align='center' style='border-collapse:collapse;color:#525252'></td>
                    </tr>
                    <tr>
                        <td valign='top' align='center' style='border-collapse:collapse;color:#525252'>
                            <table width='100%' border='0'>
                                <tbody>
                                    <tr>
                                    	<td height='34' style='border-collapse:collapse;color:#525252'></td> 
                                    </tr>
                                    <tr>
                                    	<td align='center' style='border-collapse:collapse;color:rgb(82,82,82);font-family:Arial,sans-serif;font-size:30px;font-weight:bold;line-height:120%;text-align:center' colspan='3'>
										Dalton Talento
										</td>
                                    </tr>
                                    <tr>
                                    	<td align='center' style='border-collapse:collapse;color:#525252;font-size:15px' colspan='3'></td>
                                    </tr>
                                </tbody>
                            </table>
                        
                        </td>
                    </tr>
                    <tr>
                    	<td height='38' align='center' style='border-collapse:collapse;color:#525252'></td>
                    </tr>
                   
                    <tr>
                        <td>
                            <table width='100%' style='border-spacing:0px'>
                                <tbody>
                                    <tr valign='middle'>
                                        <td width='100%' valign='middle' align='left' style='border-collapse:collapse;color:#525252;padding:10px;background-color:rgb(255,255,255);border-color:rgb(221,221,221);border-width:1px;border-bottom-left-radius:5px;border-bottom-right-radius:5px;border-style:solid;font-size:12px;padding:40px!important;vertical-align:middle'>
                                            <table cellspacing='0' cellpadding='5px' border='0'>
                                                <tbody>
					
                                                    <tr>
                                                    	<td style='border-collapse:collapse;color:#525252;padding-right:15px'><b style='color:#888;font-size:10px;text-transform:uppercase'>NOMBRE</b></td>
                                                    	<td style='border-collapse:collapse;color:#525252'>$nombre</td>
                                                    </tr>
													
                                                    <tr>
                                                    	<td style='border-collapse:collapse;color:#525252;padding-right:15px'><b style='color:#888;font-size:10px;text-transform:uppercase'>CORREO:</b></td>
                                                    	<td style='border-collapse:collapse;color:#525252'><a target='_blank' href='mailto:$correo'>$correo</a></td>
                                                    </tr>
                                                    <tr>
                                                    	<td style='border-collapse:collapse;color:#525252;padding-right:15px'><b style='color:#888;font-size:10px;text-transform:uppercase'>Teléfono:</b></td>
                                                    	<td style='border-collapse:collapse;color:#525252'>$telefono</td>
                                                    </tr>
													
                                                    <tr>
                                                    	<td style='border-collapse:collapse;color:#525252;padding-right:15px'><b style='color:#888;font-size:10px;text-transform:uppercase'>Comentarios:</b></td>
                                                    	<td style='border-collapse:collapse;color:#525252'>$comentarios</td>
                                                    </tr>
                                                    
                                                </tbody>
                                            </table>
                                        
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    <tr>
                    	<td valign='top' height='33' align='center' style='border-collapse:collapse;color:#525252'></td>
                    </tr>
                
                </tbody>
            </table>
        </td>
        </tr>
    </tbody>
</table>
</center>
</div>
	";
	
	$mail -> IsHTML (true);
	$mail -> Send ();
	$emailSent = true;	
}
?>
<?php include("includes/functions.php");?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="stylesheet" href="styles.css">
   <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
   <script src="script.js"></script>
<?php metas();?>
<title>Líderes en Desarrollo Dalton</title>
<?php scripts();?>
<?php google();?>
</head>
<body>
<?php  if($emailSent==true){ ?>
<div class="text-center relative zindex200 alert alert-success alert-dismissable">
<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
¡Muchas gracias <b><?=$nombre?></b>, tu correo se ha enviado exitosamente!
</div>
<?php  }?>
		<div id='cssmenu' class="visible-xs">
		<ul>
		   <li><a href='#section1'>PROYECTO</a></li>
		   <li><a href='#section2'>¿QUIÉN PARTICIPA?</a></li>
		   <li><a href='#section3'>PROGRAMA</a></li>
		   <li><a href='#section4'>PLAN DE CARRERA</a></li>
		</ul>
		</div>
	
		<section class="bg_header">
		<div id="section1" class="container">
			<div class="row">
				<div class="col-md-12">
					<img src="images/logo.png" class="img-responsive c_center">
				</div>
			</div>
		</div>
		<div id="Menu" class="container">
			<div class="row">
			  <div class="nav">
				  <nav>
				  <div class="col-md-6 col-xs-6">
					<a href="#section1">PROYECTO</a>
					<a href="#section2">¿QUIÉN PARTICIPA?</a>
				  </div>
				  <div class="col-md-6 col-xs-6">
					<a href="#section3">PROGRAMA</a>
					<a href="#section4">PLAN DE CARRERA</a>
				  </div>
				  </nav>
				</div>		
				
			</div>
		</div>
		</section>
		<div id="header" class="container-fluid header">
			<div class="row">
				<img src="images/banners/head.png" class="img-responsive c_center head">
			</div>
		</div>
		<section id="section1" class="bg_section02">
			<div id="line" class="container-fluid">
				<div class="row">
					<img src="images/banners/line.png" class="img-responsive line">
				</div>
		</div>
		<div id="seccion02" class="container break">
			<div class="col-md-12">
			<div class="col-md-1" class="leftd ">
				<img src="images/banners/txt01.png"><br><br>
			</div>
			<div class="col-md-11 leftd bloque01">
				<p class="parrafo">Programa de Entrenamiento para profesionistas que está destinado a captar, desarrollar y formar jóvenes con potencial de crecimiento para asumir en corto y mediano plazo cargos estratégicos dentro de la organización. Este es una de las principales puertas de entrada a la Compañía, que además ofrece un atractivo programa de desarrollo.</p>
			</div>
			</div>
		</div>
		<div id="seccion02-1" class="container break">


				  <div class="col-md-12">
			     
			     <div class="col-md-1 visible-xs leftd misionx2">
						<img src="images/banners/txt02.png">
			    	</div>
			     
			     
				     <div class="col-md-11 leftd bloque02">
						<p class="parrafo">Desarrollar futuros líderes, listos para encarar cualquier desafío, que a través de sus habilidades y conocimientos, marquen la diferencia y consigan excelentes resultados, porque estamos convencidos de que la mejor gente forma la mejor compañía.</p>
					 </div>
			    
				    <div class="col-md-1 hidden-xs leftd misionx2">
						<img src="images/banners/txt02.png">
			    	</div>
				</div>	
		 </div>
		<div id="section2" class="container">
			<div class="row">
				<div class="col-md-4 col-xs-12 bloque02" >
				<img src="images/index/texto_01.png" class="img-responsive"><br>
				<p >Es un programa de reclutamiento, selección y entrenamiento, dirigido a universitarios mexicanos recién egresados, en donde se brindará la oportunidad de ingresar a laborar y rotar por diferentes áreas del grupo, adquiriendo una constante capacitación, retos y diversas responsabilidades, para finalmente tener la oportunidad de ocupar una posición desafiante dentro de la estructura del grupo. </p>	
				</div>
				<div class="col-md-4 col-xs-12">
					<img src="images/banners/woman.png" class="img-responsive c_center">
				</div>
				<div class="col-md-4 col-xs-12 bloque02">
				<img src="images/index/texto_02.png" class="img-responsive"><br>
					<p>Durante un período de entrenamiento entre los 6 a 18 meses (dependiendo la región y área) serán responsables de diversas laborales y proyectos que los permitirán conocer nuestra empresa y las diferentes unidades de negocio.</p>

					<p>El seguimiento personalizado de la evolución de cada uno, garantizará un comienzo único en el grupo. Conocerán distintos puestos, proyectos y modelos de negocio, formarán su propia red de contactos, descubrirán nuestras divisiones y obtendrán una visión general de GRUPO DALTON, nuestra cultura y nuestro entorno laboral. 
					</p>
				</div>
			</div>
		</div>

	</section><br><br>

	<section class="bg_section03">

		<div id="section2" class="container-fluid bg_gray">
			<div class="row">
				<img src="images/banners/head.png" class="img-responsive c_center head2">
				<br><img src="images/index/texto_03.png" class="img-responsive c_center"></br></br>
			</div>
		</div>
		
		<div id="podran" class="container">
			<div class="col-md-12 podran">
			<p style="font-size: 18px; line-height: 1; padding: 25px;">Podrán participar universitarios recién egresados de hasta 25 años o bien, que tengan como máximo dos años de haberse graduado, buen promedio académico, disponibilidad para viajar, y no se requiere experiencia laboral previa.</p><br>
		   </div>
	   </div>
		   <div class="img01">
		   <img src="images/banners/imagen.png">
		   </div>
		   <div class="img02">
		   <img src="images/banners/imagen02.png">
		   </div><br>
		  <div id="centro" class="container">
			<div class="col-md-12">
				<div class="col-md-1 leftd horizontal">
						<img src="images/banners/horizontal.png" class="img-responsive"><br>
					</div><br><br>
				
					<div class="col-md-1 leftd vertical">
						<img src="images/banners/integrantes.png">
					</div>
					<div class="col-md-11 leftd bloque01">

						<div class="row">
						<div class="col-md-12 col-xs-12">
						<img src="images/banners/mentor.png" class="img-responsive asignara"><br>	
						<p class="text_01" style="font-size:  18px!important">Se asignará un mentor que apoyará, orientará respondiendo a sus preguntas y siendo un soporte durante todo su proceso como “Líder en Desarrollo Dalton”. Haciendo notar sus fortalezas y objetivos clave de aprendizaje dando la oportunidad de aplicar y perfeccionar sus competencias técnicas y habilidades en el día a día.</p>
						</div>
					</div>
					</div>
			</div>    					
	         </div>
       				<div class="container ofrecemos hidden-xs" >
						<img src="images/banners/ofrecemos.png" class="img-responsive" style="float: right; padding-right: 80px;" ><br>
						<p style="text-align: justify; float: right; font-size: 18px; line-height: 1;text-align-last: right; padding-right: 80px;">Ser parte de una de las compañías más grandes y con mayor re nombre en Guadalajara y poder participar en una de las empresas con mayor prestigio en el ramo automotriz e inmobiliario.</p><br>
						<p style="text-align: justify;float: right; font-size: 18px; line-height: 1; padding-right: 80px;">Constante capacitación y oportunidad de desarrollar habilidades adquiriendo experiencia y sólidos conocimientos desde el primer día, además de conocer y ser parte de los procesos involucrados en la cadena de valor de la industria automotriz como concesionaria e inmobiliaria como desarrolladores, en un excelente ambiente de trabajo y dentro de una cultura de informalidad.</p>
					</div>
       				
       				<div class="container ofrecemos visible-xs" >
						<img src="images/banners/ofrecemos.png" class="img-responsive" style="float: right; padding-bottom: 15px;" ><br>
						<p style="text-align: justify;float: right;font-size: 18px; line-height: 1; padding:0px 50px 0px 40px!important">Ser parte de una de las compañías más grandes y con mayor re nombre en Guadalajara y poder participar en una de las empresas con mayor prestigio en el ramo automotriz e inmobiliario.</p>
						<p style="text-align: justify;float: right; font-size: 18px; line-height: 1; padding:0px 50px 0px 40px!important">Constante capacitación y oportunidad de desarrollar habilidades adquiriendo experiencia y sólidos conocimientos desde el primer día, además de conocer y ser parte de los procesos involucrados en la cadena de valor de la industria automotriz como concesionaria e inmobiliaria como desarrolladores, en un excelente ambiente de trabajo y dentro de una cultura de informalidad.</p>
					</div>
	       				

		<div id="header" class="container-fluid bg_gray">
			<div class="row">
				<img src="images/banners/head.png" class="img-responsive c_center head2">
				<br><img src="images/index/texto_04.png" class="img-responsive c_center"></br>
			</div>
		</div>
		<div id="section3" class="container">
				<div class="row">
					<div class="col-md-12">
					   <div class="col-md-6 col-xs-12">
					   <span style="color:#ea0a2a">1.</span>Reclutamiento<br> 
					   <span style="color:#ea0a2a">2.</span>Inducción (2/3 meses)<br>
					   <span style="color:#ea0a2a">3.</span>Asignación de departamento (servicio/ staff/ comercial) y mentor <br>
					   <span style="color:#ea0a2a">4.</span>Asignación de proyecto(tiempos)
					   </div>

						<div class="col-md-6 col-xs-12 ">
						<span style="color:#ea0a2a">5.</span>Seguimiento<br> 
						<span style="color:#ea0a2a">6.</span>Presentación de<br> resultados<br>  
						<span style="color:#ea0a2a">7.</span>Plan de carrera<br> 
						</div>
					
					</div>
			</div>
		</div><br>
		
						<div class="img03">
						   <img src="images/banners/imagen03.png">
			      		 </div>

		<div id="header" class="container">
				<div class="row">
					<div class="col-md-12 col-xs-12">
				   
					   <div class="col-md-6 col-xs-12 .hidden-xs">
					  <img src="images/banners/finalizar.png" class="img-responsive"><br>
					   </div><br><br>
					   

					<div class="col-md-6 col-xs-12 puntos">
					<p><span style="color:#ea0a2a">&bull;</span>Obtener una visión global de la organización, identificar problemas y oportunidades.</p>
					<p><span style="color:#ea0a2a">&bull;</span>Incorporar herramientas para el desarrollo y la gestión de equipos y proyectos.</p>
					<p><span style="color:#ea0a2a">&bull;</span>Evaluar oportunidades de mercado, estrategias competitivas y desafíos para acompañar los cambios.</p>
					<p><span style="color:#ea0a2a">&bull;</span>Desarrollar habilidades interpersonales para maximizar el performance individual.</p>
					<p><span style="color:#ea0a2a"&bull;></span>Fortalecer la mirada estratégica y la capacidad como profesionista.</p>
					</div>
					
					</div>
			</div>
		</div><br><br>

		<div class="img01">
		   <img src="images/banners/triangulo02.png hidden-xs">
		   </div>

		   	<div class="hidden-xs" style="position: relative; float: right; margin-top: -100px;">
						   <img src="images/banners/triangulo04.png">
	    </div>	
		<div id="program" class="container program">
			<div class="row">
				<div class="col-md-12">
					<img src="images/banners/programa.png" class="img-responsive c_center"><br><br>
					<p>El programa <span style="color:#ea0a2a">LEDD </span> <strong>“Líderes en Desarrollo Dalton”</strong> cuenta con 6 – 18 meses de entrenamiento, donde los participantes, donde los participantes rotarán por las diferentes áreas de la compañía, como son: Corporativo, Inmobiliario, DES y Automotriz desarrollando diversas tareas y proyectos de mejora, los cuales deberán ser presentados ante los vicepresidentes y el director general. </p><br>
	
					<div id="graphic" class="container" style="position: relative">
					
					<div class="bloquea">
					<li>Gerencia General</li>
					<li>Comercial</li>
					<li>Servicio</li>
					<li>Administración</li>
					<li>Financiamiento y Seguros</li>
					</div>
					
					<div class="bloqueb">
					<li>Capital Humano</li>
					<li>Sistemas</li>
					<li>Contraloría</li>
					<li>Finanzas</li>
					<li>Dirección</li>
					</div>
					
					<div class="bloquec">
					<li>Dirección</li>
					<li>Operaciones</li>
					<li>Comercial</li>
					<li>Administración</li>
						</div>

					<img src="images/banners/graphic.png" class="img-responsive c_center">
				</div>
			</div>
		</div>
</div>
	

		
		<div id="procesod" class="container-fluid">
			<div class="row">
				<div class="col-md-12" class="proceso">
					<img src="images/banners/proceso.png" class="img-responsive c_center">
				</div>
			</div>
		</div>

		
		<div id="procesod2" class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<img src="images/banners/graphic02.png" class="img-responsive c_center">
				</div>
			</div>
		</div>
		
		
		<div id="section4" class="container-fluid bg_gray">
			<div class="row">
				<img src="images/banners/head.png" class="img-responsive c_center head2">
				<br><img src="images/banners/plan.png" class="img-responsive c_center"></br>
			</div>
		</div>
	
		<div class="img01">
		   <img src="images/banners/triangulo03.png" class="img-responsive">
		   </div>	

		<div id="program" class="container program">
			<div class="row">
				<div class="col-md-12">
					<p>Luego de los 18 meses de entrenamiento, los <span style="color:#ea0a2a">LEDD</span> serán asignados a sus primeras posiciones dentro de la estructura de Grupo Dalton.</p>

					<p>El objetivo de esta primera asignación es consolidar los conocimientos adquiridos en las etapas previas y comenzar a construir la base para futuras posiciones, contando con capacitación constante del área de Capital Humano, así como con un plan de gestión de carrera.</p>
				</div>
			</div>
		</div>
		

			<div id="section1" class="container">
			<div class="container information">
      	<div class="row">
      		<div class="col-md-12" id="contacto" name="contacto"> 
      		 <img src="images/banners/contacto.png" class="img-responsive c_center"></br><br>
				<form  name="newsletter" method="post" action="index.php" data-toggle="validator" role="form" class="validate">
					<div class="form-group">
						<input type="text" class="form-control" name="nombre" id="z_name" placeholder="Nombre: *" required class="reveal">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="telefono" id="z_phone" placeholder="Teléfono: *" required class="reveal">
					</div>
					<div class="form-group">
						<input type="email" class="form-control" name="correo" id="z_requester" placeholder="Correo electrónico: *" required class="reveal">
					</div>
					<div class="form-group">
						<textarea class="form-control" id="z_comments" name="comentarios" rows="3" placeholder="Comentarios" required class="reveal"></textarea>
					</div>
					<div class="text-center">
						<input type="submit" name="submit" value="Enviar" class="reveal btn btn-danger">
					</div>
				</form><br><br>

      		
      		
       		</div>
        </div>
      
		</div>
</div>
			

				</div>
			</div>


	</section>
	
	<div id="header" class="container-fluid bg_red">
			<div class="row">
			
			</div>
		</div>


 <script src="js/slideNav.js" type="text/javascript"></script>
    <script>
	    window.slide = new SlideNav({
		changeHash: true    
	    });

    </script>



</body>
</html>
